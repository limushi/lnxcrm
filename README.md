# Об этой программе (rus)

* исходная задача требует записи контактов в одном месте
* изначальное решение это хранение файлов в текстовом файле
* затем, когда потребуется анализ или сверка контактов, система интегрирует БД

# About this program (en)

* this is a basic crm for personal use
* it's can save, find and report about contacts in my incoming messages or phone calls
* send reviews to [Facebook group](https://www.facebook.com/groups/linuxoffice/)

# Local configs

* windows
~/Documents/shell/crm
* linux
~/crm

Пока это будет добавление новых контактов и напоминалка о встречах с ними. 
